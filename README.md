# example-rest

Simple REST Controller example in Spring Boot.

Spring Boot project Initialized with:
https://start.spring.io

## Running App

```
mvn clean spring-boot:run
```

## App REST API

### Get Users

Get all users:
```
curl -i  http://localhost:8080/api/users
```
Response:
```
HTTP/1.1 200 
Content-Type: application/json
[
  {"id":1,"name":"user1","phone":"1111","email":"user1@example.com"},
  {"id":2,"name":"user2","phone":"2222","email":"user2@example.com"}
]
```

### Get User By ID

Get User with ID=1:
```
curl -i  http://localhost:8080/api/users/1
```
Response:
```
HTTP/1.1 200 
Content-Type: application/json
{
  "id": 1,
  "name": "user1",
  "phone": "1111",
  "email": "user1@example.com"
}
```

Get User with unknown ID:
```
curl -i  http://localhost:8080/api/users/777
```
Response HTTP 404 (Not Found):
```
HTTP/1.1 404
User not found
```

### Create User (POST method)

Create new User:
```
curl -i -X POST http://localhost:8080/api/users \
-H 'Content-Type: application/json' \
-d '{"name": "user3", "phone": "3333", "email": "user3@example.com"}'
```
Response:
```
HTTP/1.1 201
{
  "id": 3,
  "name": "user3",
  "phone": "3333",
  "email": "user3@example.com"
}
```

Create User with invalid parameters:
```
curl -i -X POST http://localhost:8080/api/users \
-H 'Content-Type: application/json' \
-d '{"name": "user4", "phone": "4444", "email": "invalid-email"}'
```
Response HTTP 400 (Bad Request):
```
HTTP/1.1 400 
{ "error":"Bad Request", ... }
```

### Update User (PUT method)

Update User with ID=1:
```
curl -i -X PUT http://localhost:8080/api/users/1 \
-H 'Content-Type: application/json' \
-d '{"name": "user1", "phone": "1234", "email": "user1234@example.com"}'
```
Response:
```
HTTP/1.1 200
{
  "id": 1,
  "name": "user1",
  "phone": "1234",
  "email": "user1234@example.com"
}
```

### Delete User (DELETE method)

Delete User with ID=2:
```
curl -i -X DELETE http://localhost:8080/api/users/2
```
Response:
```
HTTP/1.1 204 
```

Delete User with unknown ID:
```
curl -i -X DELETE http://localhost:8080/api/users/777
```
Response HTTP 404 (Not Found):
```
HTTP/1.1 404 
User not found
```
