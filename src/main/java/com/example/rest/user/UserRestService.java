package com.example.rest.user;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class UserRestService {

    private final List<User> users = new ArrayList<>();

    @PostConstruct
    protected void init() {
        log.info("Adding 2 users");
        users.add(new User(1, "user1", "1111", "user1@example.com"));
        users.add(new User(2, "user2", "2222", "user2@example.com"));
    }

    public List<User> getUsers() {
        return users;
    }

    public User getUserById(int id) {
        return users.stream()
                .filter(user -> user.getId() == id)
                .findFirst()
                .orElseThrow(() -> new UserNotFoundException("User not found"));
    }

    public User createUser(User user) {
        log.info("Create user: {}", user);
        int maxId = users.stream()
                .mapToInt(User::getId)
                .max()
                .orElse(0);
        user.setId(maxId + 1);
        users.add(user);
        return user;
    }

    public User updateUser(int id, User data) {
        log.info("Update user: {}, {}", id, data);
        User user = getUserById(id);
        user.setName(data.getName());
        user.setPhone(data.getPhone());
        user.setEmail(data.getEmail());
        return user;
    }

    public void deleteUser(int id) {
        log.info("Delete user: {}", id);
        boolean deleted = users.removeIf(user -> user.getId() == id);
        if (!deleted) {
            throw new UserNotFoundException("User not found");
        }
    }
}
