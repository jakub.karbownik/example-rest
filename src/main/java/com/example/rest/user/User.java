package com.example.rest.user;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
@AllArgsConstructor
public class User {

    @Min(0)
    private int id;
    @NotBlank
    @Size(min = 4, max = 40)
    private String name;
    @NotBlank
    @Size(min = 4, max = 20)
    private String phone;
    @NotBlank
    @Email
    @Size(min = 4, max = 40)
    private String email;
}
